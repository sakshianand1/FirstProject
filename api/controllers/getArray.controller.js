var async=require('async');
var getDataController = require('../controllers/getData.controller');
//Get Array
module.exports.getProjects = (req,res)=>{
    var projects = [];
    
   console.log("getProjects"+req.params.user+req.params.token);
     getDataController.projectArr(req.params.user,req.params.token,(err,result)=>{
        if(err){
            console.err(err);
            return;
        }
        
        projects=result;
        //console.log(projects[0]);
        res.send(projects[0])
    })
}
module.exports.getArrayData = (req,res)=>{
   // console.log(req.params);
    var stack = {};
    
    stack.commitArr = (callback)=>{
        var commits = [];
        getDataController.getCommit(req.params.id,req.params.token,(err,result)=>{
            if(err){
                console.err(err);
                return;
            }
            commits=result;
            callback(null,commits[0]);
           // console.log(result);
        });   
       
    }
    stack.branchArr = (callback)=>{
        var branches = []
         getDataController.getBranches(req.params.id,req.params.token,(err,result)=>{
            if(err){
                console.err(err);
                return;
            }
            branches=result;
            callback(null,branches[0]);
            //console.log(result);
        });
        
        
    }
    stack.mergeArr = (callback)=>{
        var mergeArr = [];
        getDataController.getMergeReq(req.params.id,req.params.token,(err,result)=>{
            if(err){
                console.err(err);
                return;
            }
            mergeArr=result;
            callback(null,mergeArr[0]);
           // console.log(mergeArr[0][0][0].id);
        });
        
    }
    
    
    async.parallel(stack,(err,result)=>{
        if(err){
            console.err(err);
            return;
        }
        res.send(result)
    })
} 




