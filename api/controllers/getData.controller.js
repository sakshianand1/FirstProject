//Logic goes here
var axios = require('axios');
var async=require('async');


module.exports.projectArr = (user,token,callback)=>{
   
// console.log("token"+token);
if(!token){
    token='';
}
 let projectArr = [];
 axios.get('https://gitlab.com/api/v4/users/'+user+'/projects?owned=true&private_token='+token)
 .then(response=>{
     projectArr.push(response.data);
     callback(null,projectArr);
 })
 .catch(err=>{
    console.log("project axios",err.message);
})
}
//Get commits
module.exports.getCommit = (id,token,callback)=>{
    if(!token){
        token='';
    }
    let commitArr =[];
    axios.get('https://gitlab.com/api/v4/projects/'+id+'/repository/commits?per_page=200&private_token='+token)
    .then(response=>{  
        commitArr.push(response.data)
        callback(null,commitArr);
       
    })
    .catch(err=>{
        console.log("commit axios",err.message);
    })
    
    
}

//Get Branches
module.exports.getBranches = (id,token,callback)=>{
    if(!token){
        token='';
    }
    let branchArr = [];
    axios.get('https://gitlab.com/api/v4/projects/'+id+'/repository/branches?private_token='+token)
    .then(response=>{  
        branchArr.push(response.data);
        callback(null,branchArr);
    })
    .catch(err=>{
        console.log("branch axios",err.message);
    })
}

//Get merge Requests
module.exports.getMergeReq = (id,token,callback)=>{
    if(!token){
        token='';
    }
    let mergeArr = [];
    axios.get('https://gitlab.com/api/v4/projects/'+id+'/merge_requests?private_token='+token)
    .then(response=>{
        mergeArr.push(response.data);
        callback(null,mergeArr);
    })
    .catch(err=>{
        console.log("merge axios",err.message);
    })
    
}
