//Routing goes here
var express = require('express');
var router = express.Router(); //creating router
var getArrayController = require('../controllers/getArray.controller');

router
.get('/getProjects/:user',getArrayController.getProjects)
.get('/getProjects/:user/:token',getArrayController.getProjects)
.get('/:id',getArrayController.getArrayData)
.get('/:id/:token',getArrayController.getArrayData)



module.exports=router;