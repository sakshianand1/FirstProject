const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const path=require('path');
//const getData = require('./routes/api');
const routes = require('./api/routes/index.js');


app.use('/api/',routes);//When it is /api then it will go to getData



app.use('/',express.static(path.join(__dirname,'/demoapp/build/')))

app.listen(port,()=>{
    console.log('listening from port 5000');
}).on('error', function(err){
    console.log('on error handler');
    console.log(err.message);
});




