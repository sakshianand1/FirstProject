import React, { Component } from 'react';
import {Form, Button} from 'semantic-ui-react';
import {Link,Route} from 'react-router-dom';
class UserForm extends Component {
render() {
    
    return (
        <div>
        {
           // !this.state.submit && 
             <Form  error onSubmit={this.props.submit}>
             <Form.Input id="user" style={{width:"40%"}} label='User name' placeholder='Enter User Name' onChange={this.props.handleChange} />
             <Form.Checkbox label='Show Private Repositories' checked={this.props.showPrivate} onChange={this.props.handleSelect}/>
             {
               this.props.showPrivate && 
               <Form.Input id="token" style={{width:"40%"}} label='Token' placeholder='Enter Personal Access Token' onChange={this.props.handleChange} />
             }
             {
               !this.props.showPrivate && 
               <Form.Input style={{width:"40%"}} label='Token' placeholder='Enter Personal Access Token' disabled />
             }
               {/* <Button type="submit"><Link to="/showRepositories" onClick={this.props.submit}>Show Repositories</Link></Button> */}
               <Button type="submit"><Link to="/showRepositories" onClick={this.props.submit}>Show Repositories</Link></Button>
               </Form> 
           }
        </div>
    );
}

}
export default UserForm;