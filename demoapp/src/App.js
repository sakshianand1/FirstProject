import React, { Component } from 'react';
import Chart from './barChart.js'
import {Form, Icon, Button,Menu,Container} from 'semantic-ui-react';
import {Link,Route} from 'react-router-dom';
import UserForm from './form.js';
import './App.css';

class App extends Component {
  state={
    option:'',
    id:'',
    dataArr:[],
    mergeArr:[],
    branchArr:[],
    input:"",
    showPrivate:false,
    token:'',
    submit:false
  }
  
  
  // Count the number of commits
 
  countCommits=(arr,d)=>{
   let count = arr.map(entry=>d.filter(commit=>commit.author_name===entry).length)
   return count;
  }

  // Fetches the name of all  distinct committers

  distinctCommiter=(d)=>{
    let commiters=[...new Set(d.map(commiter=>commiter.author_name))];
    return commiters;
   
  }
  onSelect=(event,{value})=> {
    console.log('heloo');
    this.setState({option:{value}});
  }

  handleSelect=()=>{
    console.log('hello');
    this.setState((state)=>({showPrivate:!state.showPrivate}));
  }
  handleChange=(e)=>{
    if(e.target.id=="user") {
      this.setState({input:e.target.value  })
    }
    else if(e.target.id=="token") {
      this.setState({token:e.target.value  })
    }
    
   
  }
  submit=()=> {
    console.log('hi');
    // if(this.state.input==''||(this.state.showPrivate==true && this.state.password=="")) {
    //   console.log('error');
    // }
    // else {
    this.setState((state)=>({submit:!state.submit}));
    
    // }
  }
 close=()=>{
  this.setState((state)=>({submit:!state.submit,token:'',input:''}));
 }
  render() {
     console.log(this.state.submit);
    const {value} = this.state.option;
    
    const options = [
      { value: 1, text: 'Number of Commits' },
      { value: 2, text: 'Contributors' },
      { value: 3, text: 'Pull Requests' },
      {value: 4, text:'Number of Branches'}
     ];
     var repo=this.state.input;
    return (
      
      <div className="App">
      <Container fluid style={{background:"black",color:"white"}}>
       <div>
            <h2> Gitlab Analytics Tool </h2>
            {this.state.submit && <Link to='/'><Icon name="arrow alternate circle left outline" size="big" onClick={this.close}/></Link>}
       </div>
      </Container>
      <Container fluid >
      
     

    <Route exact path='/' render={()=>(
      <UserForm handleChange={this.handleChange} submit={this.submit} textAlign='center' handleSelect={this.handleSelect} showPrivate={this.state.showPrivate}/>
    )}/>
    <Route path='/showRepositories' render={()=>(
      
      <Chart key={this.state.input&&this.state.token} user={this.state.input} token={this.state.token} showPrivate={this.state.showPrivate} option={this.state.option}  countCommits={this.countCommits} distinctCommiter={this.distinctCommiter} />
      


    )}/>
     </Container>
     <Container fluid style={{background:"black",color:"white", bottom:"0",position:"fixed"}}>   
       <h2> Wipro </h2>
    </Container>    
      </div>
    );
  }
}

export default App;
