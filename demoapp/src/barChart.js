import React, { Component } from 'react';
import {Bar} from 'react-chartjs-2';
import {Line} from 'react-chartjs-2';
import {Link} from 'react-router-dom';

import { List, Dimmer, Loader, Button, Grid, Menu, Segment } from 'semantic-ui-react';

import axios from 'axios';
class Chart extends Component {
state= {

    id:'',
    dataArr:[],
    mergeArr:[],
    branchArr:[],
    projectArr:[],
    projectID:''
}
handleClick=(event)=> {
    console.log(event.target.id);
    this.setState({projectID:event.target.id});
    this.getData(event.target.id);

}
componentDidMount = ()=>{
   
        axios.get('/api/getProjects/'+this.props.user+'/'+this.props.token).then(res=>{
            console.log(res.data)
            this.getData(res.data[0].id)
            this.setState({projectArr:res.data,projectID:res.data[0].id});
             
             console.log(this.state.projectArr);
         })
         .catch(err=>console.log("Client Project Axios error"+err.message))
 


  
}

getData=(id)=>{
  
        axios.get('/api/'+id+'/'+this.props.token).then(res=>{
            console.log(res.data);
            this.setState({dataArr:res.data.commitArr,mergeArr:res.data.mergeArr,branchArr:res.data.branchArr,projectID:id})
        })
        .catch(err=>console.log("Client Data Axios Error"+err.message));
    
    
}
render() {
    
    console.log(this.state.dataArr);
    var dataCommit,dataCont,dataPull;
    var recentCommitDate, start14Commit;
    var w=window.screen.width;
    var h=window.screen.height;
    if( this.state.dataArr.length>0) {
        recentCommitDate = new Date(''+this.state.dataArr[0].committed_date+'');
        start14Commit = new Date(recentCommitDate.setDate(recentCommitDate.getDate()-14));
        let xAxisCommit = [];
        let xLabelCommit=[];
        for(let i=0;i<14;i++)
        {
            xAxisCommit.push(new Date(start14Commit.setDate(start14Commit.getDate()+1)));
            xLabelCommit.push(xAxisCommit[i].toLocaleDateString());
        }
        let yAxisCommit=[];
        yAxisCommit = xAxisCommit.map(current=>this.state.dataArr.filter(commit=>new Date(''+commit.created_at+'').toLocaleDateString()===current.toLocaleDateString() ).length)
        dataCommit = {
            labels: xLabelCommit,
            datasets: [
                {
                    label: 'Commits',
                    backgroundColor: 'rgba(255,99,132,0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                    hoverBorderColor: 'rgba(255,99,132,1)',
                    data: yAxisCommit
                }
            ]
        };
        var xAxisCont = this.props.distinctCommiter(this.state.dataArr);
        var yAxisCont= this.props.countCommits(xAxisCont,this.state.dataArr);
        dataCont = {
            labels: xAxisCont,
            datasets: [
                {
                    label: 'Contributions',
                    backgroundColor: 'rgba(255,99,132,0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                    hoverBorderColor: 'rgba(255,99,132,1)',
                    data: yAxisCont
                }
            ]
        };
    }
    if(this.state.mergeArr.length>0) {
        recentCommitDate = new Date(''+this.state.mergeArr[0].created_at+'');
        start14Commit = new Date(recentCommitDate.setDate(recentCommitDate.getDate()-14));
        let xAxisPull = [];
        let xLabelPull=[];
        for(let i=0;i<14;i++)
        {
            xAxisPull.push(new Date(start14Commit.setDate(start14Commit.getDate()+1)));
            xLabelPull.push(xAxisPull[i].toLocaleDateString());
        }
        let yAxisPull = xAxisPull.map(current=>this.state.mergeArr.filter(commit=>new Date(''+commit.created_at+'').toLocaleDateString()===current.toLocaleDateString() ).length)
        console.log(yAxisPull);
        dataPull = {
            labels: xLabelPull,
            datasets: [
                {
                    label: 'Pull Requests',
                    backgroundColor: 'rgba(255,99,132,0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                    hoverBorderColor: 'rgba(255,99,132,1)',
                    data: yAxisPull
                }
            ]
        };

    }
    return (
        <div className="chart">
        
        <Grid>
            <Grid.Column width={3}>
                <Menu fluid vertical tabular>
                    <Menu.Item header><h4>{this.props.user}'s Repositories</h4></Menu.Item>
                    

                    {this.state.projectArr.length>0 && 
                        this.state.projectArr.map(project=> 
                        <Menu.Item id={project.id} key={project.id} name={project.name} active={this.state.projectID === project.id} onClick={this.handleClick} />)
                    }

                </Menu>
            </Grid.Column>

            <Grid.Column stretched width={10}>
            <Segment>

                {
                    this.state.projectArr.length==0 && 
                    <Dimmer active>
                    <Loader />
                    </Dimmer>

                }    
                                
                <div>
                {this.state.mergeArr.length===0 && 
                <div>
                <div className="ui stackable one column grid">
                <div className="column">
                {
                    dataCommit && this.state.dataArr.length>0 && 

                    <Line 
                        width={0.9*w}
                        height={0.5*h}
                        data={dataCommit}
                    />
                }
                </div>
                </div>
                <div className="ui stackable one column grid">
                <div className="column">
                {
                    dataCont && this.state.dataArr.length>0 &&
                    <Bar
                        data={dataCont}
                        width={0.9*w}
                        height={0.5*h}
                        options={{
                        maintainAspectRatio: true
                    }}
                    />
                }
                </div>
                </div>
                <div className="ui stackable one column grid">
                <div className="column">
                {
                    this.state.mergeArr.length>0 && dataPull && 
                    <Line 
                        width={w}
                        height={h}
                        data={dataPull}
                    />
                }
                </div>
                </div>
                


                </div>
                }
                {this.state.mergeArr.length>0 && 
                <div>
                <div className="ui stackable two column grid">
                <div className="column">
                {
                    dataCommit && this.state.dataArr.length>0 && 

                    <Line 
                        width={w}
                        height={h}
                        data={dataCommit}
                    />
                }
                </div>
                <div className="column">
                {
                    dataCont && this.state.dataArr.length>0 &&
                    <Bar
                        data={dataCont}
                        width={w}
                        height={h}
                        options={{
                        maintainAspectRatio: true
                        }}
                    />
                }
                </div>
                </div>
                <div className="ui stackable two column grid centered">
                <div className="column" textAlign='center'>
                {
                this.state.mergeArr.length>0 && dataPull && 
                <Line 
                width={w}
                height={h}
                data={dataPull}
                />
                }
                </div>

                {/* <div className="column">
                {
                    this.state.branchArr.length>0 && 
                
                    <List>
                        <List.Header><h4>Branches</h4></List.Header>
                    {  this.state.branchArr.map(branch=>
                            <List.Item> {branch.name }</List.Item>
                        )
                    }
                    </List>
                    
                }
                </div> */}
                </div>
                </div>
                } 
                </div>


            </Segment>
            </Grid.Column>
            <Grid.Column width={3}>
            <div className="column">
                {
                    this.state.branchArr.length>0 && 
                
                    <List>
                        <List.Header><h4>Branches</h4></List.Header>
                    {  this.state.branchArr.map(branch=>
                            <List.Item key={branch.name}> {branch.name }</List.Item>
                        )
                    }
                    </List>
                    
                }
            </div>
            </Grid.Column>
        </Grid>


        </div>
    )
}
}
export default Chart;